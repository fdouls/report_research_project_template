\contentsline {section}{\numberline {1}Introduction}{3}{section.1}% 
\contentsline {section}{\numberline {2}Lorenz system}{3}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Model definition}{3}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Mathematical formulation}{3}{subsection.2.2}% 
\contentsline {subsection}{\numberline {2.3}Fixed points}{4}{subsection.2.3}% 
\contentsline {subsection}{\numberline {2.4}Numerical simulation}{4}{subsection.2.4}% 
\contentsline {subsection}{\numberline {2.5}Results}{5}{subsection.2.5}% 
\contentsline {section}{\numberline {3}Predator Prey Model}{5}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Model definition}{5}{subsection.3.1}% 
\contentsline {subsection}{\numberline {3.2}Mathematical formulation}{6}{subsection.3.2}% 
\contentsline {subsection}{\numberline {3.3}Fixed points}{6}{subsection.3.3}% 
\contentsline {subsection}{\numberline {3.4}Numerical simulation}{7}{subsection.3.4}% 
\contentsline {subsection}{\numberline {3.5}Results}{8}{subsection.3.5}% 
\contentsline {section}{\numberline {4}Conclusion}{9}{section.4}% 
\contentsline {section}{\numberline {5}Optional topic $ : { $Poiseuille flow$ } $ }{10}{section.5}% 
\contentsline {section}{\numberline {6}References}{12}{section.6}% 
